package citi.trading.dao;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import citi.trading.trading.Equity;
import junit.framework.Assert;

/**
 * 
 * @author Mia
 * EquityDAO Test
 *
 */
public class EquityDAOTest {
	private EquityDAO equityDAO;
	HttpURLConnection conMock;
	
	@Before
	public void init(){
		equityDAO = new EquityDAO();
		conMock = Mockito.mock(HttpURLConnection.class);
	}
	
	@Test
	public void whenNewStockSymbolAppearsAddToMap() throws Exception{
		String input = "2018-07-17 8:13:15.123, 12.53,18.45,14.33,19.20,13";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		Mockito.when(conMock.getInputStream()).thenReturn(in);	
		
		equityDAO.readStockData("HELLO", 10);
		Assert.assertTrue(equityDAO.getEquities().containsKey("HELLO"));
		//Assert.assertEquals(, actual);
	}
	
	@Test
	public void whenStockSymbolExistsShouldUpdateMap() throws Exception{
		// Arrange
		HashMap<String, ArrayList<Equity>> equityMock = new HashMap<String, ArrayList<Equity>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		Timestamp dummyTime = new Timestamp(sdf.parse("2018-07-15 8:13:15.123").getTime());
		System.out.println(dummyTime.toString());
		Equity dummyEquity = new Equity("HELLO", 12.5, 14.3, 18.7, 16.6, 23.2, dummyTime);
		ArrayList<Equity> equityLst = new ArrayList<Equity>();
		equityLst.add(dummyEquity);
		equityMock.put("HELLO", equityLst);
		
		String input = "2018-07-17 8:13:15.456,12.53,18.45,14.33,19.20,13";
		InputStream in = new ByteArrayInputStream(input.getBytes());
		Mockito.when(conMock.getInputStream()).thenReturn(in);
		// Act
		equityDAO.readStockData("HELLO", 8);
	}
	

}
