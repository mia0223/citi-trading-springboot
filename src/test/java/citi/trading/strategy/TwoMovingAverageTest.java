package citi.trading.strategy;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

/**
 * 
 * @author Mia
 * TwoMovingAverage Test
 *
 */
public class TwoMovingAverageTest {
	private TwoMovingAverage twoMovingAvg;
	
	@Before
	public void init(){
		twoMovingAvg = new TwoMovingAverage(5,10);
	}
	
	@Test
	public void whenCrossOverBeingCalledOnTMAReturnsResult(){
		for(int i=0; i<40; i++){
			twoMovingAvg.crossover((double) i);
		}
		String outcome = twoMovingAvg.crossover(12.8);
		Assert.assertEquals("BUY", outcome);
	}

}
