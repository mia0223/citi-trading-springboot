package citi.trading.strategy;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

/**
 * 
 * @author Mia
 * MovingAverage Test
 *
 */
public class MovingAverageTest {
	MovingAverage movingAvg;
	
	@Before
	public void init(){
		movingAvg = new MovingAverage(30);
	}
	
	@Test(expected = Exception.class)
	public void shouldThrowExceptionWhenSizeIsLessThanBufferLength() throws Exception{
		movingAvg.average();
	}
	
	@Test
	public void whenPriceBingPushedBufferShouldContainIt(){
		movingAvg.push(105.8);
		Assert.assertTrue(movingAvg.getBuffer().contains(105.8));
	}
	
	@Test
	public void averageShouldCalculateCorretAvg() throws Exception{
		List<Double> buffer = new LinkedList<Double>();
		for(int i=0; i<30; i++){
			buffer.add((double) i);
		}
		movingAvg.setBuffer(buffer);
		double outcome = movingAvg.average();
		Assert.assertEquals(14.5, outcome);
	}
}
