package citi.trading.trading;

import java.util.HashMap;

/**
 * 
 * @author Mia
 * Singleton pattern to keep a global hash map storing all active traders.
 *
 */
public class TraderCollection {
	private static TraderCollection traderCollection = null;
	private HashMap<String, Trader> traderMap;
	
	private TraderCollection(){
		traderMap = new HashMap<String, Trader>();
	}
	
	public static TraderCollection getTraderCollection(){
		if(traderCollection == null)
			return new TraderCollection();
		else
			return traderCollection;
	}
	
	public HashMap<String, Trader> getTraderMap(){
		return traderMap;
	}
	
	public void addTrader(String key, Trader value){
		traderMap.put(key, value);
	}
	
	public void removeTrader(String key){
		traderMap.remove(key);
	}
}
