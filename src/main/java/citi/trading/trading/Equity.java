package citi.trading.trading;

import java.sql.Timestamp;

/**
 * 
 * @author Angela
 * Equity class to keep all info of each equity
 *
 */
public class Equity {

	private String symbol;
	private double openPrice;
	private double high;
	private double low;
	private double closePrice;
	private double volumn;
	private Timestamp time;

	public Equity(String symbol, double openPrice,double high,double low, double closePrice, double volumn, Timestamp time) {

		this.symbol = symbol;
		this.openPrice = openPrice;
		this.high = high;
		this.low = low;
		this.closePrice = closePrice;
		this.volumn = volumn;
		this.time = time;

	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(double openPrice) {
		this.openPrice = openPrice;
	}

	public double getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(double closePrice) {
		this.closePrice = closePrice;
	}

	public double getVolumn() {
		return volumn;
	}

	public void setVolumn(double volumn) {
		this.volumn = volumn;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}