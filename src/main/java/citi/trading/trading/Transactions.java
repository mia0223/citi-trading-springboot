package citi.trading.trading;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Majd
 * JPA transaction table object
 *
 */
@Entity(name = "Transactions")
@Table(name = "Transactions")
public class Transactions {

    @Id
    @Column(name = "transactionID")
    private int trxID;

    @Column(name = "equityName")
    private String equityName;

    @Column(name = "transactionType")
    private String trxType;

    @Column(name = "equityAmount")
    private int equityAmount;

    @Column(name = "equityPrice")
    private double equityPrice;

    @Column(name = "traderID")
    private String traderID;

    @Column(name = "strategy")
    private String strategy;

    @Column(name = "strategyParameters")
    private String strategyParameters;

    @Column(name = "profit")
    private double profit;
    
 

    @Column(name = "timeStamp")
    @Temporal(TemporalType.DATE)
    private Date timeStamp;

    public int getTrxID() {
        return trxID;
    }

    public void setTrxID(int trxID) {
        this.trxID = trxID;
    }

    public String getEquityName() {
        return equityName;
    }

    public void setEquityName(String equityName) {
        this.equityName = equityName;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public int getEquityAmount() {
        return equityAmount;
    }

    public void setEquityAmount(int equityAmount) {
        this.equityAmount = equityAmount;
    }

    public double getEquityPrice() {
        return equityPrice;
    }

    public void setEquityPrice(double equityPrice) {
        this.equityPrice = equityPrice;
    }

    public String getTraderID() {
        return traderID;
    }

    public void setTraderID(String traderID) {
        this.traderID = traderID;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getStrategyParameters() {
        return strategyParameters;
    }

    public void setStrategyParameters(String strategyParameters) {
        this.strategyParameters = strategyParameters;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
