package citi.trading.trading;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import citi.trading.dao.EquityDAO;
import citi.trading.dao.TransactionDAO;
import citi.trading.message.handling.MyMessageHandler;
import citi.trading.strategy.Strategy;
import citi.trading.strategy.TwoMovingAverage;
/**
 * 
 * @author Angela
 * Object that use strategy to trade and write transactions records to database
 *
 */

public class Trader {
	private String decision;
	private String stockSymbol;
	private String strategyName;
	private TransactionDAO trxDAO;
	private boolean isTrading;
	private ArrayList<Double> stockPrices;
	private MyMessageHandler mmh = new MyMessageHandler();
	private HashMap<String,String> parameters;
	private AtomicLong traderID;
	private double profit;
	private int amount;
	private int trxID;
	private EquityDAO dataFetcher = new EquityDAO();
	private TwoMovingAverage tMA;



	public Trader(String stockSymbol, String strategyName, int amount,HashMap<String,String> parameters,AtomicLong tdID) {
	
		//generate constructor
		isTrading = true;	
		this.parameters = parameters;
		stockPrices= new ArrayList<Double>();
		ApplicationContext appCxt = new ClassPathXmlApplicationContext("config.xml");
    	trxDAO = (TransactionDAO) appCxt.getBean("transactionDAO");
		traderID = tdID;
		this.strategyName = strategyName;
		this.amount = amount;
		this.stockSymbol = stockSymbol;
		tMA = new TwoMovingAverage(Integer.valueOf(parameters.get("long")),Integer.valueOf(parameters.get("short")));
		

	}
	
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}

	public TransactionDAO getTrxDAO() {
		return trxDAO;
	}
	public void setTrxDAO(TransactionDAO trxDAO) {
		this.trxDAO = trxDAO;
	}
	public void setIsTrading(boolean s){
		this.isTrading = s;
	}
	public boolean getIsTrading(){
		return isTrading;
	}
	public boolean checkInput(){
		try {
			trade();
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		return true;

	}

	public String runTwoMA(String longTime, String shortTime, double price)throws Exception{


		this.decision = tMA.crossover(price);
	
		return decision;



	}
	public void buy(int  id, double price){

		 String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				 String text = "<trade>\r\n" + 
						"  <buy>true</buy>\r\n" + 
						"  <id>"+id+"</id>\r\n" + 
						"  <price>"+price+"</price>\r\n" + 
						"  <size>"+amount+"</size>\r\n" + 
						"  <stock>"+stockSymbol+"</stock>\r\n" + 
						"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
						"</trade>";
				mmh.makeMessage(text, traderID.toString());
				System.out.println(text);

	}
	public void sell(int  id, double price){
		String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				 String text = "<trade>\r\n" + 
						"  <buy>false</buy>\r\n" + 
						"  <id>"+id+"</id>\r\n" + 
						"  <price>"+price+"</price>\r\n" + 
						"  <size>"+amount+"</size>\r\n" + 
						"  <stock>"+stockSymbol+"</stock>\r\n" + 
						"  <whenAsDate>"+ time + "</whenAsDate>\r\n" + 
						"</trade>";
				mmh.makeMessage(text, traderID.toString());
				System.out.println(text);


	}
	public void trade()throws Exception {


		
		
		while(isTrading) {
			List<Transactions> buying = trxDAO.getBuyByTraderID(traderID.toString());
			List<Transactions> selling = trxDAO.getSellByTraderID(traderID.toString());
			double buyPrice=0;
			double sellPrice=0;
			for(Transactions t:buying) {
				buyPrice+=t.getEquityPrice();
				
			}
			for(Transactions t:selling) {
				sellPrice+=t.getEquityPrice();
				
			}
			double loss = Math.abs((buyPrice-sellPrice))/buyPrice*100;
			profit = Math.abs(sellPrice-buyPrice)/sellPrice*100;

			if(loss>=Double.valueOf(parameters.get("loss"))||profit<=Double.valueOf(parameters.get("profit"))) {
				isTrading =false;
				
			}
		dataFetcher.readStockData(stockSymbol,Integer.valueOf(parameters.get("long")));
	
		for (int i = 0;isTrading&&i<((ArrayList<Equity>) (dataFetcher.getEquities().get(stockSymbol))).size();i++ ) {
		
			stockPrices.add(new Double(((ArrayList<Equity>) dataFetcher.getEquities().get(stockSymbol)).get(i).getClosePrice()));
		
		}

		//run first round data with strategy

		for (Double d:stockPrices) {

			decision = runTwoMA(parameters.get("long"),parameters.get("short"),Double.valueOf(d));
		
			if(decision.equals("BUY")){
				trxID =(int) trxDAO.countTransactions()+3;
				buy(trxID,d);				
				decision = " ";			
			}
			else if(decision.equals("SELL")){
				trxID =(int) trxDAO.countTransactions()+3;
				sell(trxID,d);	
				decision = " ";
			}
			else{
				decision = " ";
				continue;
			}

		}

		TimeUnit.SECONDS.sleep(15);
		}
					
				System.out.println("shut down");
				TraderCollection trc = TraderCollection.getTraderCollection();
				trc.removeTrader(traderID.toString());
		
	}

	//Receive msg and write it into database

	public void trListener(String msg){
		 try {
			 Transactions tr = new Transactions();
			 DocumentBuilderFactory dbf =
			 DocumentBuilderFactory.newInstance();
			 DocumentBuilder db = dbf.newDocumentBuilder();
			 InputSource is = new InputSource();
			 is.setCharacterStream(new StringReader(msg));

			 Document doc = db.parse(is);
			 NodeList nodes = doc.getElementsByTagName("trade");

       
			 Element element = (Element) nodes.item(0);

			 NodeList res = element.getElementsByTagName("result");
			 Element line = (Element) res.item(0);
			 String result = getCharacterDataFromElement(line);
			 System.out.println(result);

			 if(result.equals("FILLED")){
				 tr.setTraderID(traderID.toString());
				 tr.setStrategy("TwoMovingAverage");

				 NodeList buy = element.getElementsByTagName("buy");
				 line = (Element) buy.item(0);
				 if(Boolean.valueOf(getCharacterDataFromElement(line))) {
					 tr.setTrxType("BUY");
				 }
				 else {
					 tr.setTrxType("SELL");
				 }
             
           NodeList id = element.getElementsByTagName("id");
           line = (Element) id.item(0);
           tr.setTrxID(Integer.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));


           NodeList EquityName = element.getElementsByTagName("stock");
           line = (Element) EquityName.item(0);
           tr.setEquityName(getCharacterDataFromElement(line));
           System.out.println(getCharacterDataFromElement(line));

           
           NodeList price = element.getElementsByTagName("price");
           line = (Element) price.item(0);
           tr.setEquityPrice(Double.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));

           NodeList size = element.getElementsByTagName("size");
           line = (Element) size.item(0);
           tr.setEquityAmount(Integer.valueOf(getCharacterDataFromElement(line)));
           System.out.println(getCharacterDataFromElement(line));


           //timestamp
           SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");


           NodeList wADate = element.getElementsByTagName("whenAsDate");
           line = (Element)wADate .item(0);
           Date date = format.parse(getCharacterDataFromElement(line));
           tr.setTimeStamp(date);
           System.out.println(date);
           
        

           //parameters
          tr.setStrategyParameters(parameters.get("short")+parameters.get("long")+parameters.get("profit")+parameters.get("loss"));
       
          tr.setProfit(profit);
          System.out.println(this.stockSymbol);
          
           trxDAO.insertTransaction(tr);

       }
       else if(result.equals("PARTIALLY_FILLED")){

       }
       else{

       }


    }
    catch (Exception e) {
        e.printStackTrace();
    }
   
    
  }

  public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
       CharacterData cd = (CharacterData) child;
       return cd.getData();
    }
    return "?";
  }

	
}