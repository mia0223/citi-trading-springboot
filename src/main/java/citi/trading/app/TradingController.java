package citi.trading.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import citi.trading.dao.TransactionDAO;
import citi.trading.trading.Trader;
import citi.trading.trading.TraderCollection;
import citi.trading.trading.Transactions;

/**
 * 
 * @author Mia
 * Controller class for reset api
 *
 */
@RestController
public class TradingController {

    private final AtomicLong counter = new AtomicLong();
    private TraderCollection traders = TraderCollection.getTraderCollection();
    private Trader trader;
    @Autowired
    private TransactionDAO trxDAO;
    private static final Logger LOGGER = Logger.getLogger( TradingController.class.getName() );
    
    @CrossOrigin(origins="localhost:9080")
    @RequestMapping(value="/getStrategy", method=RequestMethod.GET)
    public String getStrategy(@RequestParam Map<String, String> strategyMap) throws JSONException {
    	counter.incrementAndGet();
    	
    	String stock = strategyMap.get("stock");
    	int amount = Integer.parseInt(strategyMap.get("amount"));
    	HashMap<String, String> params = new HashMap<String, String>();
    	params.put("short", strategyMap.get("short"));
    	params.put("long", strategyMap.get("long"));
    	params.put("profit", strategyMap.get("profit"));
    	params.put("loss", strategyMap.get("loss"));
    	
    	Trader trader = new Trader(stock, "TwoMovingAverage", amount, params, counter);
    	traders.addTrader(counter.toString(), trader);
    	
    	System.out.println("stock is " +stock + ", amount is " + amount + ", short = " + strategyMap.get("short")
    	+ ", long = " + strategyMap.get("long") + ", profit = " + strategyMap.get("profit") + ", loss = " + strategyMap.get("loss"));
    	
    	LOGGER.info("stock is " +stock + ", amount is " + amount + ", short = " + strategyMap.get("short")
     	+ ", long = " + strategyMap.get("long") + ", profit = " + strategyMap.get("profit") + ", loss = " + strategyMap.get("loss"));
    	
    	JSONObject confirmation = new JSONObject();
    	trader.checkInput();
    	
		confirmation.put("message", "Successful");
		JSONArray array = new JSONArray();
		JSONObject item = new JSONObject(strategyMap);
		item.put("strategy", "TwoMovingAverage");
		item.put("traderID", counter.toString());
		array.put(item);
		confirmation.put("info", array);
		
    	return confirmation.toString();
    }
   
    @CrossOrigin(origins="localhost:9080")
    @RequestMapping(value="/getCommand", method=RequestMethod.GET)
    public String getCommand(@RequestParam(value="traderID") String id) throws Exception{
    	System.out.println(id);
    	LOGGER.info("traderID --- " + id);
    	JSONObject confirmation = new JSONObject();
    	trader = traders.getTraderMap().get(id);
    	if(trader != null){
    		System.out.println("trying to kill");
    		LOGGER.info("trying to kill");
    		trader.setIsTrading(false);
    		traders.removeTrader(id);
    		confirmation.put("message", "Stopped");   				
    	}
    	else{
    		confirmation.put("message", "Trader ID not found");
    	}
    	return confirmation.toString();    
    }
    
    @CrossOrigin(origins="localhost:9080")
    @RequestMapping(value="/isActive", method=RequestMethod.GET)
    public String isActive(@RequestParam(value="traderID") String id) throws JSONException{
    	trader = traders.getTraderMap().get(id);
    	JSONObject msg = new JSONObject();
    	if(trader != null){
    		msg.put("isActive", trader.getIsTrading());
    		msg.put("message", true);
    	}
    	else{
    		msg.put("message", false);
    	}
    	return msg.toString();
    }
	
    @CrossOrigin(origins="localhost:9080")
    @RequestMapping(value="/listActive", method=RequestMethod.GET)
    public String listActive() throws Exception{
    	if(!traders.getTraderMap().isEmpty()){
    		ArrayList<String> array = new ArrayList<String>(traders.getTraderMap().keySet());
    		return new Gson().toJson(array);
    	}
    	else{
    		ArrayList<String> emptyArray = new ArrayList<String>();
    		return new Gson().toJson(emptyArray);
    	}
    }
    
//    // TODO Needs to be checked on VM database
//    @RequestMapping(value="/getHistoryByStock", method=RequestMethod.GET)
//    public String getHistoryByStock(@RequestParam(value="filterByStock") String stock){
//    	List<Transactions> list = trxDAO.loadTransactionByStrategy(stock);
//    	String result = new Gson().toJson(list);
//    	//trxDAO.close();
//    	return result;
//    }
//    
//    //date in format yyyy-MM-dd --> checked
//    @RequestMapping(value="getHistoryByDate", method=RequestMethod.GET)
//    public String getHistoryByDate(@RequestParam(value="filterByDate") String date) throws ParseException{
//    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//    	Date input = sdf.parse(date);
//    	List<Transactions> list = trxDAO.loadTransactionByDate(input);
//    	String result = new Gson().toJson(list);
//    	//trxDAO.close();
//    	return result;
//    }

    @CrossOrigin(origins="localhost:9080")
    @RequestMapping(value="/getAll", method=RequestMethod.GET)
    public String getHistoryAll() throws Exception{
		List<Transactions> list = trxDAO.loadAll();
		String result = new Gson().toJson(list);
		//trxDAO.close();
		return result;   	
    }
}
