package citi.trading.strategy;

/**
 * 
 * @author Mia
 * Abstract base class for all strategies 
 *
 */
public abstract class Strategy {
    private String title;

    private String parameters;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }
}
