package citi.trading.strategy;

/**
 * 
 * @author Mia
 * Implementation of TwoMovingAverage strategy.
 * Takes two different times to calculate different average and make trading decision
 */
public class TwoMovingAverage extends Strategy {
	private MovingAverage small;
	private MovingAverage large;
	
	public TwoMovingAverage(int shortTime, int longTime) {
		int shortSize = shortTime * 4;
		int longSize = longTime * 4;
		this.small = new MovingAverage(shortSize);
		this.large = new MovingAverage(longSize);
	}
	
	public String crossover(double price){
		small.push(price);
		large.push(price);
		
		try{
			double shortAverage = small.average();
			double longAverage = large.average();	
			
			System.out.println ("shortAvg" + shortAverage);
			System.out.println ("longAvg" + longAverage);
			if(shortAverage > longAverage){
				return "BUY";
			}
			else
				return "SELL";
		}
		catch(Exception e){
			return "Waiting for more values";
		}
	}
}
