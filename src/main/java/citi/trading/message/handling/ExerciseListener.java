package citi.trading.message.handling;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * 
 * @author Majd
 *
 */
public class ExerciseListener {
	public static void main(String[] args) {
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:spring-beans.xml");
		ctx.refresh();

		while (true) {
		}
	}
}
