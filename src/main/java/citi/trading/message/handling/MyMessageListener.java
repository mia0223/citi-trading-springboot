package citi.trading.message.handling;

import java.util.logging.Logger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import citi.trading.trading.Trader;
import citi.trading.trading.TraderCollection;

/**
 * 
 * @author Majd & Rami
 * JMS Listener instance to listen message from order broker
 */
public class MyMessageListener implements MessageListener {
	private TraderCollection traders = TraderCollection.getTraderCollection();
	private static final Logger LOGGER = Logger.getLogger( MyMessageListener.class.getName() );
	
	public void onMessage(Message m) {
		System.out.println( "In Listener");
				
		TextMessage message = (TextMessage) m;
		try {
     		System.out.println(message.getText());
     		String[] output = message.getJMSCorrelationID().split(",");
     		System.out.println("trader id " + output[1]);
     		LOGGER.info("trader id " + output[1]);
        	Trader tr = traders.getTraderMap().get(output[1]);
     		tr.trListener(message.getText());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
