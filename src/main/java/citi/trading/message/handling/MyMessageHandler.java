package citi.trading.message.handling;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

/**
 * 
 * @author Majd & Rami & Angela
 * JMS object to send message to order broker
 *
 */
public class MyMessageHandler {
	
	private ApplicationContext context;
	private JmsTemplate jmsTemplate;
	private Destination destination;
	private Destination replyTo;
	
	private static final Logger LOGGER = Logger.getLogger( MyMessageHandler.class.getName() );
	
	public MyMessageHandler() {
		context = new ClassPathXmlApplicationContext("spring-beans.xml");
		destination = (Destination) context.getBean("destination", Destination.class);
		replyTo = (Destination) context.getBean("replyTo", Destination.class);
		jmsTemplate = (JmsTemplate) context.getBean("messageSender");
	}

	public void makeMessage(final String trade, String traderID) {
		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				 String time = OffsetDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				 String correlationID = time + "," + traderID;
				 LOGGER.info(correlationID);
				TextMessage tm = session.createTextMessage();
				tm.setText(trade);
				tm.setJMSCorrelationID(correlationID);
				tm.setJMSReplyTo(replyTo);
				return tm;
			}	
			
		});

	}
}