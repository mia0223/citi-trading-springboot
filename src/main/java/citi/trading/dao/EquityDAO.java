package citi.trading.dao;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Logger;

import citi.trading.app.TradingController;
import citi.trading.trading.Equity;

/**
 * 
 * @author Angela
 * Data access object for Equity
 *
 */
public class EquityDAO {
	private HashMap<String,ArrayList<Equity>> equities;
	private Equity stock;
	private InputStream inputStream;
	private static final Logger LOGGER = Logger.getLogger( TradingController.class.getName() );
	
	public EquityDAO() {
		equities = new HashMap<String,ArrayList<Equity>>();
	}

	public Equity getStock() {
		return stock;
	}


	public void setStock(Equity stock) {
		this.stock = stock;
	}


	public HashMap<String, ArrayList<Equity>> getEquities() {
		return equities;
	}



	public void setEquities(HashMap<String, ArrayList<Equity>> equities) {
		this.equities = equities;
	}



	public void readStockData(String stockSymbol, int period)throws Exception {
		try {
            URL url = new URL("http://incanada1.conygre.com:9080/prices/"+stockSymbol+"?periods="+period);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            System.out.println("Connected :)");
            LOGGER.info("Connected :)");
            inputStream = connection.getInputStream();
            Scanner sc = new Scanner(inputStream);
            sc.nextLine();
            //first time write all data into hashmap

            if(!equities.containsKey(stockSymbol)) {
            	equities.put(stockSymbol,new ArrayList<Equity>());
	            while(sc.hasNext()) {
	            	String data = sc.nextLine();
	            	System.out.println(data);
	            	LOGGER.info(data);
	
	            	String[] stockData = data.split(",");
	            	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.SSS");
	            	Date parsedDate = dateFormat.parse(stockData[0]);
	            	Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
	            	
	            	Equity newPrice = new Equity(stockSymbol,Double.parseDouble(stockData[1]),Double.parseDouble(stockData[2]),Double.parseDouble(stockData[3]),Double.parseDouble(stockData[4]),Double.parseDouble(stockData[5]),timestamp);
	            	equities.get(stockSymbol).add(newPrice);
	            }
            }

            //only update the last row of data
            else {
            	String data = "";
            	String[] stockData= new String[7];
            	 while(sc.hasNext()) {
            		 data = sc.nextLine();
                  	 System.out.println(data);
                  	 LOGGER.info(data);
                 	 stockData = data.split(",");
            	 }

             	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.SSS");
             	Date parsedDate = dateFormat.parse(stockData[0]);
             	Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

             	Equity newPrice = new Equity(stockSymbol,Double.parseDouble(stockData[1]),Double.parseDouble(stockData[2]),Double.parseDouble(stockData[3]),Double.parseDouble(stockData[4]),Double.parseDouble(stockData[5]),timestamp);
         		//System.out.println(Double.parseDouble("in"));
             	//we only keep track of 300 prices of a stock
             	if(equities.get(stockSymbol).size()>300) {
             		equities.get(stockSymbol).add(newPrice);
             		equities.get(stockSymbol).remove(0);
             	}
             	else {
             		equities.get(stockSymbol).add(newPrice);
             	} 
            }
            sc.close();
		}
	
		catch (MalformedURLException e) {
            e.printStackTrace();
        } 
		catch (IOException e) {
            e.printStackTrace();
        }
        finally {
        }


	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}




}
