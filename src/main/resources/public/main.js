(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/active-strategies/active.strategies.component.html":
/*!********************************************************************!*\
  !*** ./src/app/active-strategies/active.strategies.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nTemplate for monitoring and terminating active strategies\n@Author Majd Asab + Rami Shehto\n-->\n<div class=\"container\">\n  <div class=\"form-group\">\n    <label for=\"active-strategies\">Strategies active at the moment:</label>\n    <select class=\"form-control\" id=\"active-strategies\" (click)=\"refreshList()\" (change)=\"strategy=$event.target.value\">\n      <option *ngFor=\"let f of array\" [value]=\"f\">{{f}}</option>\n      <option selected=\"selected\"></option>\n    </select>\n    <button type=\"button\" class=\"btn btn-danger\"  [disabled]=\"strategy === undefined\" (click)=\"killStrategy()\" >Kill active strategy</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/active-strategies/active.strategies.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/active-strategies/active.strategies.component.ts ***!
  \******************************************************************/
/*! exports provided: ActiveStrategiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActiveStrategiesComponent", function() { return ActiveStrategiesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _serviceCall__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../serviceCall */ "./src/app/serviceCall.ts");
/*
Component to handle active strategies monitoring and termination.
@Author Majd Asab + Rami Shehto
*/
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ActiveStrategiesComponent = /** @class */ (function () {
    function ActiveStrategiesComponent(serviceCall) {
        this.serviceCall = serviceCall;
        this.array = [];
        this.killActive = true;
    }
    ActiveStrategiesComponent.prototype.killStrategy = function () {
        this.serviceCall.killCommand(this.strategy).subscribe(function (data) { return console.log("Kill:", data); }, function (error) { return console.log("kill error:", error); });
        this.strategy = undefined;
    };
    ActiveStrategiesComponent.prototype.refreshList = function () {
        var _this = this;
        this.serviceCall.refreshActiveStrategiesList().subscribe(function (data) {
            if (data.length <= 0) {
                console.log("Insufficient number of strategies");
            }
            else {
                _this.array = data;
            }
        }, function (error) { return console.log("Strategies update error", error); });
    };
    ActiveStrategiesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "active-strategies-component",
            template: __webpack_require__(/*! ./active.strategies.component.html */ "./src/app/active-strategies/active.strategies.component.html")
        }),
        __metadata("design:paramtypes", [_serviceCall__WEBPACK_IMPORTED_MODULE_1__["ServiceCall"]])
    ], ActiveStrategiesComponent);
    return ActiveStrategiesComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n.parent-div{\n  height:50vh;\n}\n.div-1{\n  display: inline-block;\n  width:50%;\n  height:100%;\n  opacity:1;\n  transition: 0.5s ease;\n  color:white;\n}\n.colorit{\n  background-color: #E5EDFF;\n}\nh1{\n  color:red;\n}\n/*.div-1:hover{*/\n/*!*opacity: 0.3;*!*/\n/*!*color: black;*!*/\n/*}*/\n.text-middle{\n  /*position: absolute;*/\n  /*top: 50%;*/\n  /*left: 50%;*/\n  text-align: center;\n}\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nTemplate to render main page.\n@Author Majd Asab + Rami Shehto\n-->\n<div class=\"container colorit\">\n  <div class=\"jumbotron text-center colorit\">\n    <img src=\"../assets/logoooo.PNG\" width=\"250\" height=\"200\">\n  </div>\n\n\n  <h3>Choose operation:</h3>\n  <select [(ngModel)]=\"Strategies\" class=\"form-control\">\n    <option value=\"1\">Submit strategy</option>\n    <option value=\"2\">View transactions</option>\n    <option value=\"3\">Active strategies</option>\n  </select>\n\n  <div [ngSwitch]=\"Strategies\">\n    <div *ngSwitchCase=\"1\">\n      <two-moving-averages-form></two-moving-averages-form>\n    </div>\n    <div *ngSwitchCase=\"2\">\n      <transactions-table></transactions-table>\n    </div>\n    <div *ngSwitchCase=\"3\">\n      <active-strategies-component></active-strategies-component>\n    </div>\n    <div *ngSwitchDefault>\n      <two-moving-averages-form></two-moving-averages-form>\n    </div>\n  </div>\n\n\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form/form.component */ "./src/app/form/form.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _serviceCall__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./serviceCall */ "./src/app/serviceCall.ts");
/* harmony import */ var _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./transactions/transactions.component */ "./src/app/transactions/transactions.component.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _active_strategies_active_strategies_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./active-strategies/active.strategies.component */ "./src/app/active-strategies/active.strategies.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _form_form_component__WEBPACK_IMPORTED_MODULE_4__["FormComponent"],
                _transactions_transactions_component__WEBPACK_IMPORTED_MODULE_7__["TransactionsComponent"],
                _active_strategies_active_strategies_component__WEBPACK_IMPORTED_MODULE_9__["ActiveStrategiesComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_8__["Ng2SmartTableModule"],
            ],
            providers: [_serviceCall__WEBPACK_IMPORTED_MODULE_6__["ServiceCall"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/form/dummyModel.ts":
/*!************************************!*\
  !*** ./src/app/form/dummyModel.ts ***!
  \************************************/
/*! exports provided: DummyModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DummyModel", function() { return DummyModel; });
/*
Class used as a base model for strategy parameters' inputs.
@Author Majd Asab + Rami Shehto
*/
var DummyModel = /** @class */ (function () {
    function DummyModel(equity, amount, long, short, profitCap, lossCap) {
        this.equity = equity;
        this.amount = amount;
        this.long = long;
        this.short = short;
        this.profitCap = profitCap;
        this.lossCap = lossCap;
    }
    return DummyModel;
}());



/***/ }),

/***/ "./src/app/form/form.component.css":
/*!*****************************************!*\
  !*** ./src/app/form/form.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ng-valid[required], .ng-valid.required  {\n  border-left: 5px solid #42A948; /* green */\n}\n\n.ng-invalid:not(form)  {\n  border-left: 5px solid #a94442; /* red */\n}\n\nbutton{\n  margin: 5px;\n}\n\n.kill{\n  float:right;\n}\n"

/***/ }),

/***/ "./src/app/form/form.component.html":
/*!******************************************!*\
  !*** ./src/app/form/form.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nTemplate for rendering strategy form.\n@Author Majd Asab + Rami Shehto\n-->\n<div class=\"container\">\n  <h2>Submit strategy</h2>\n\n  <form (ngSubmit)=\"onSubmit()\" #twoMovingAverages=\"ngForm\">\n\n    <div class=\"form-group\">\n      <label for=\"equity\">Equity</label>\n      <select class=\"form-control\" id=\"equity\" [(ngModel)]=\"model.equity\" name=\"equity\" #name=\"ngModel\" (change)=\"equity=$event.target.value\" required >\n        <option value=\"GE\">General Electric (GE)</option>\n        <option value=\"BAC\">Bank of America (BAC)</option>\n        <option value=\"DDE\">Dover Downs Gaming&Entertainment (DDE)</option>\n        <option value=\"CLF\">Cleveland-Cliffs (CLF)</option>\n        <option value=\"HAL\">Halliburton (HAL)</option>\n        <option value=\"T\">AT&T (T)</option>\n        <option value=\"F\">Ford Motor (F)</option>\n        <option value=\"CHK\">Chesapeake Energy (CHK)</option>\n        <option value=\"TWTR\">Twitter (TWTR)</option>\n        <option value=\"FCX\">Freeport-McMoRan (FCX)</option>\n        <option value=\"ABEV\">Ambev ADR (ABEV)</option>\n        <option value=\"ABX\">Barrick Gold (ABX)</option>\n        <option value=\"JCP\">J.C. Penney (JCP)</option>\n        <option value=\"VALE\">Vale ADR (VALE)</option>\n        <option value=\"PBR\">Petroleo Brasileiro ADR (PBR)</option>\n        <option value=\"WFC\">Wells Fargo (WFC)</option>\n        <option value=\"PFE\">Pfizer (PFE)</option>\n        <option value=\"BABA\">Alibaba Group Holding ADR (BABA)</option>\n        <option value=\"C\">Citigroup (C)</option>\n        <option value=\"GGP\">GGP (GGP)</option>\n        <option value=\"JPM\">JPMorgan Chase (JPM)</option>\n        <option value=\"RF\">Regions Financial (RF)</option>\n        <option value=\"ORCL\">Oracle (ORCL)</option>\n        <option value=\"SKX\">Skechers USA Cl A (SKX)</option>\n        <option value=\"KR\">Kroger (KR)</option>\n        <option value=\"NBR\">Nabors Industries (NBR)</option>\n        <option value=\"AKS\">AK Steel Holding (AKS)</option>\n        <option value=\"NOK\">Nokia ADR (NOK)</option>\n        <option value=\"VER\">VEREIT (VER)</option>\n        <option value=\"DNR\">Denbury Resources (DNR)</option>\n        <option value=\"AUY\">Yamana Gold (AUY)</option>\n        <option value=\"ITUB\">Itau Unibanco Holding ADR (ITUB)</option>\n        <option value=\"SWN\">Southwestern Energy (SWN)</option>\n        <option value=\"BMY\">Bristol-Myers Squibb (BMY)</option>\n        <option value=\"KEY\">KeyCorp (KEY)</option>\n        <option value=\"BK\">Bank of New York Mellon (BK)</option>\n        <option value=\"APRN\">Blue Apron Holdings Cl A (APRN)</option>\n        <option value=\"ESV\">ENSCO (ESV)</option>\n        <option value=\"SAN\">Banco Santander ADR (SAN)</option>\n        <option value=\"FCAU\">Fiat Chrysler Automobiles (FCAU)</option>\n        <option value=\"MRO\">Marathon Oil (MRO)</option>\n        <option value=\"SQ\">Square Cl A (SQ)</option>\n        <option value=\"VZ\">Verizon Communications (VZ)</option>\n        <option value=\"X\">United States Steel (X)</option>\n        <option value=\"SNAP\">Snap (SNAP)</option>\n        <option value=\"AA\">Alcoa (AA)</option>\n        <option value=\"FIT\">Fitbit (FIT)</option>\n        <option value=\"XOM\">Exxon Mobil (XOM)</option>\n        <option value=\"PAH\">Platform Specialty Products (PAH)</option>\n        <option value=\"M\">Macy's (M)</option>\n        <option value=\"IBN\">ICICI Bank ADR (IBN)</option>\n        <option value=\"RIG\">Transocean (RIG)</option>\n        <option value=\"GGB\">Gerdau ADR (GGB)</option>\n        <option value=\"PBRA\">Petroleo Brasileiro ADR A (PBRA)</option>\n        <option value=\"TSM\">Taiwan Semiconductor Manufacturing ADR (TSM)</option>\n        <option value=\"KO\">Coca-Cola (KO)</option>\n        <option value=\"GG\">Goldcorp (GG)</option>\n        <option value=\"SLB\">Schlumberger (SLB)</option>\n        <option value=\"CTL\">CenturyLink (CTL)</option>\n        <option value=\"STT\">State Street (STT)</option>\n        <option value=\"GM\">General Motors (GM)</option>\n        <option value=\"BB\">BlackBerry (BB)</option>\n        <option value=\"PM\">Philip Morris International (PM)</option>\n        <option value=\"USB\">U.S. Bancorp (USB)</option>\n        <option value=\"DIS\">Walt Disney (DIS)</option>\n        <option value=\"V\">VISA Cl A (V)</option>\n        <option value=\"JMEI\">Jumei International Holding ADR (JMEI)</option>\n        <option value=\"DWDP\">DowDuPont (DWDP)</option>\n        <option value=\"MS\">Morgan Stanley (MS)</option>\n        <option value=\"WFT\">Weatherford International (WFT)</option>\n        <option value=\"ITW\">Illinois Tool Works (ITW)</option>\n        <option value=\"RAD\">Rite Aid (RAD)</option>\n        <option value=\"KMI\">Kinder Morgan (KMI)</option>\n        <option value=\"ABBV\">AbbVie (ABBV)</option>\n        <option value=\"CGC\">Canopy Growth (CGC)</option>\n        <option value=\"COG\">Cabot Oil&Gas (COG)</option>\n        <option value=\"DB\">Deutsche Bank (DB)</option>\n        <option value=\"STM\">STMicroelectronics (STM)</option>\n        <option value=\"WTI\">W&T Offshore (WTI)</option>\n        <option value=\"PG\">Procter&Gamble (PG)</option>\n        <option value=\"LPI\">Laredo Petroleum (LPI)</option>\n        <option value=\"COP\">ConocoPhillips (COP)</option>\n        <option value=\"NE\">Noble (NE)</option>\n        <option value=\"MRK\">Merck&Co (MRK)</option>\n        <option value=\"CVS\">CVS Health (CVS)</option>\n        <option value=\"HPE\">Hewlett Packard Enterprise (HPE)</option>\n        <option value=\"ABT\">Abbott Laboratories (ABT)</option>\n        <option value=\"NCLH\">Norwegian Cruise Line Holdings (NCLH)</option>\n        <option value=\"HL\">Hecla Mining (HL)</option>\n        <option value=\"TEVA\">Teva Pharmaceutical Industries ADR (TEVA)</option>\n        <option value=\"HTZ\">Hertz Global Holdings (HTZ)</option>\n        <option value=\"HUYA\">HUYA ADR (HUYA)</option>\n        <option value=\"SLCA\">U.S. Silica Holdings (SLCA)</option>\n        <option value=\"BHGE\">Baker Hughes a GE (BHGE)</option>\n        <option value=\"BSX\">Boston Scientific (BSX)</option>\n        <option value=\"NLY\">Annaly Capital Management (NLY)</option>\n        <option value=\"OMC\">Omnicom Group (OMC)</option>\n        <option value=\"MT\">ArcelorMittal ADR (MT)</option>\n        <option value=\"WMB\">Williams (WMB)</option>\n        <option value=\"VIPS\">Vipshop Holdings ADR (VIPS)</option>\n      </select>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"amount\">Amount (USD)</label>\n      <input type=\"number\" class=\"form-control\" id=\"amount\" name=\"model.amount\" [(ngModel)]=\"model.amount\" #name=\"ngModel\" (input)=\"amount=$event.target.value\" required >\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"short\">Short (min)</label>\n      <input type=\"number\" class=\"form-control\" id=\"short\" name=\"model.short\" [(ngModel)]=\"model.short\" #name=\"ngModel\" (input)=\"short=$event.target.value\" required>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"long\">Long (min)</label>\n      <input type=\"number\" class=\"form-control\" id=\"long\"  name=\"model.long\" [(ngModel)]=\"model.long\" #name=\"ngModel\" (input)=\"long=$event.target.value\" required>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"profitCap\">Profit Cap (%)</label>\n      <input type=\"number\" class=\"form-control\" id=\"profitCap\" name=\"model.profitCap\" [(ngModel)]=\"model.profitCap\" #name=\"ngModel\" min=\"0.01\" max=\"100\" (input)=\"profitCap=$event.target.value\" required>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"lossCap\">Loss Cap (%)</label>\n      <input type=\"number\" class=\"form-control\" id=\"lossCap\" min=\"0.01\" max=\"100\" name=\"model.lossCap\" [(ngModel)]=\"model.lossCap\" #name=\"ngModel\" (input)=\"lossCap=$event.target.value\" required>\n    </div>\n\n    <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!twoMovingAverages.form.valid\">Submit</button>\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"this.twoMovingAverages.reset()\">Reset</button>\n    <!--<button type=\"button\" class=\"btn btn-danger kill\" [disabled]=\"!killActive\" (click)=\"killStrategy()\" >Kill active strategy</button>-->\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/form/form.component.ts":
/*!****************************************!*\
  !*** ./src/app/form/form.component.ts ***!
  \****************************************/
/*! exports provided: FormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComponent", function() { return FormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _serviceCall__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../serviceCall */ "./src/app/serviceCall.ts");
/* harmony import */ var _dummyModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dummyModel */ "./src/app/form/dummyModel.ts");
/*
Component for handling strategy submissions.
@Author Majd Asab + Rami Shehto
*/
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FormComponent = /** @class */ (function () {
    function FormComponent(serviceCall) {
        this.serviceCall = serviceCall;
        this.url = 'http://dev5.conygre.com:9080';
        this.killActive = false;
        this.model = new _dummyModel__WEBPACK_IMPORTED_MODULE_2__["DummyModel"]("C", 1, 3, 2, 4, 5);
        this.isValid = false;
        this.equity = "C";
        this.amount = 1;
        this.short = 2;
        this.long = 3;
        this.profitCap = 4;
        this.lossCap = 5;
    }
    FormComponent.prototype.onSubmit = function () {
        // alert(this.equity+this.amount+this.short+this.long+this.profitCap+this.lossCap);
        this.serviceCall.sendData(this.equity, this.amount, this.short, this.long, this.profitCap, this.lossCap)
            .subscribe(function (data) { return console.log("Form submitted successfully:", data); }, function (error) { return console.log('Form submission error', error); });
    };
    FormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'two-moving-averages-form',
            template: __webpack_require__(/*! ./form.component.html */ "./src/app/form/form.component.html"),
            styles: [__webpack_require__(/*! ./form.component.css */ "./src/app/form/form.component.css")]
        }),
        __metadata("design:paramtypes", [_serviceCall__WEBPACK_IMPORTED_MODULE_1__["ServiceCall"]])
    ], FormComponent);
    return FormComponent;
}());



/***/ }),

/***/ "./src/app/serviceCall.ts":
/*!********************************!*\
  !*** ./src/app/serviceCall.ts ***!
  \********************************/
/*! exports provided: ServiceCall */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceCall", function() { return ServiceCall; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
Service component to handle http requests needed.
@Author Majd Asab + Rami Shehto
*/


var ServiceCall = /** @class */ (function () {
    function ServiceCall(http) {
        this.http = http;
        this.url = 'http://dev5.conygre.com:9080';
    }
    // form submission call
    ServiceCall.prototype.sendData = function (equity, amount, short, long, profitCap, lossCap) {
        return this
            .http
            .get(this.url + "/getStrategy?stock=" + equity + "&amount=" + amount + "&short=" + short + "&long=" + long + "&profit=" + profitCap + "&loss=" + lossCap);
    };
    // database fetching call
    ServiceCall.prototype.getAllDbInfo = function () {
        return this.http.get(this.url + "/getAll");
    };
    // strategy termination call
    ServiceCall.prototype.killCommand = function (traderId) {
        return this.http.get(this.url + "/getCommand?traderID=" + traderId);
    };
    // active strategies query call
    ServiceCall.prototype.refreshActiveStrategiesList = function () {
        return this.http.get(this.url + "/listActive");
    };
    ServiceCall = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ServiceCall);
    return ServiceCall;
}());



/***/ }),

/***/ "./src/app/transactions/transactions.component.html":
/*!**********************************************************!*\
  !*** ./src/app/transactions/transactions.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nTemplate to render database information in a table.\n@Author Majd Asab + Rami Shehto\n-->\n<br>\n<button type=\"button\" class=\"btn btn-info\" (click)=\"refresh()\">Refresh</button>\n<ng2-smart-table\n  [settings]=\"settings\"\n  [source]=\"jsonData\">\n</ng2-smart-table>\n"

/***/ }),

/***/ "./src/app/transactions/transactions.component.ts":
/*!********************************************************!*\
  !*** ./src/app/transactions/transactions.component.ts ***!
  \********************************************************/
/*! exports provided: TransactionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsComponent", function() { return TransactionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _serviceCall__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../serviceCall */ "./src/app/serviceCall.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*
Component to handle fetching of database information.
@Author Majd Asab + Rami Shehto
*/


var TransactionsComponent = /** @class */ (function () {
    function TransactionsComponent(serviceCall) {
        this.serviceCall = serviceCall;
        this.settings = {
            actions: false,
            columns: {
                trxID: {
                    title: 'ID'
                },
                equityName: {
                    title: 'Equity'
                },
                equityAmount: {
                    title: 'Amount'
                },
                equityPrice: {
                    title: "Price"
                },
                trxType: {
                    title: "Transaction type"
                },
                strategy: {
                    title: "Strategy"
                },
                strategyParameters: {
                    title: "Strategy Paramters"
                },
                profit: {
                    title: "Profit"
                },
                timeStamp: {
                    title: "Timestamp"
                }
            }
        };
    }
    TransactionsComponent.prototype.ngOnInit = function () {
        this.refresh();
    };
    TransactionsComponent.prototype.refresh = function () {
        var _this = this;
        this.serviceCall.getAllDbInfo().subscribe(function (data) {
            _this.jsonData = data;
            console.log("Refresh successful");
        }, function (error) { return console.log('Database info fetching error', error); });
    };
    TransactionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'transactions-table',
            template: __webpack_require__(/*! ./transactions.component.html */ "./src/app/transactions/transactions.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [_serviceCall__WEBPACK_IMPORTED_MODULE_1__["ServiceCall"]])
    ], TransactionsComponent);
    return TransactionsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/majd/Desktop/sbTest/newnew/citi-trading-springboot/src/main/resources/ui/frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map